using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Character
{

    public int ExperienceReward { get; set; }
    public int GoldReward { get; set; }
    public Character Target { get; private set; }

    public Enemy(int maxHealth, float speed, int maxMana, int experienceReward, int goldReward)
    {
        this.MaxHealth = maxHealth;
        this.CurrentHealth = this.MaxHealth;

        this.Speed = speed;

        this.MaxMana = maxMana;
        this.CurrentMana = this.MaxMana;

        this.ExperienceReward = experienceReward;
        this.GoldReward = goldReward;

        this.Level = 1;
        this.Experience = 0;

        this.MovementType = new AiMovement();

        Ability = new Ability(1, 10, 5, 0, 0);
    }

    public Enemy Clone() {
        Enemy monster = (Enemy)this.MemberwiseClone();
        monster.GetGameObjectComponents();
        UpdateCaller.AddUpdateCallback(Update);
        return monster;
    }

    protected override void Update()
    {
        if (this.IsDead())
        {
            if (Anim)
            {
                Anim.SetBool("isDead", true);
            }
            Agent.isStopped = true;
        }
        else
        {
            MovementType.Update();
            GetGameObjectComponents();
            CanAttackTarget();
        }
    }

    public void SetNavigationTarget(Transform target)
    {
        if (this.Agent)
        {
            this.Agent.enabled = true;
            this.Agent.SetDestination(target.position);
            this.CharacterObj.transform.LookAt(target.position);
        }
    }
    protected void CanAttackTarget()
    {
        if (Agent.remainingDistance < Ability.range)
        {
            if (Target != null) {
                this.UseAbility(Ability, Target);
            }
            Anim.SetBool("isAttacking", true);
        }
        else
        {
            Anim.SetBool("isAttacking", false);
        }
    }

    public void SetAttackTarget(Character character) {

        this.Target = character;
    }

    public void RemoveUpdate()
    {
        UpdateCaller.RemoveUpdateCallback(Update);
    }
}