using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Character
{
    public int CurrentHealth { get; set; }
    public int MaxHealth { get; set; }
    public float Speed { get; set; }
    public int Level { get; set; }
    public int CurrentMana { get; set; }
    public int MaxMana { get; set; }
    public int Experience { get; set; }
    public Movement MovementType { get; set; }

    // sp�cifique unity
    protected NavMeshAgent Agent { get; set; }
    protected Animator Anim { get; set; }
    public GameObject CharacterObj { get; set; }

    public Ability Ability { get; set; }

    protected abstract void Update();
        //StartCoroutine(HealthRegeneration());
        //StartCoroutine(ManaRegeneration()); 


    protected void ReceiveDamage(int amount) {
        
        int newHealth = CurrentHealth - amount;

        if (newHealth >= 0)
        {
            CurrentHealth = newHealth;
        }
        else {
            CurrentHealth = 0;
        }
    }

    protected void ReceiveHealth(int amount) {

        int newHealth = CurrentHealth + amount;

        if (newHealth <= MaxHealth)
        {
            CurrentHealth = newHealth;
        }
        else
        {
            CurrentHealth = MaxHealth;
        }
    }

    protected void ReceiveMana(int amount) {

        int newMana = CurrentMana + amount;

        if (newMana <= MaxMana)
        {
            CurrentMana = newMana;
        }
        else
        {
            CurrentMana = MaxMana;
        }
    }

    protected void NextLevel() {

        Level += 1;
    }

    public bool IsDead() 
    {        
        return CurrentHealth <= 0;
    }

    protected void UseAbility(Ability ability, Character target) {

        if (Ability.cooldownCounter <= 0)
        {
            ability.LaunchAbility();
            target.ReceiveDamage(ability.damage);
        }
        else 
        {
            Ability.cooldownCounter -= Time.deltaTime;
        }
    }

    protected IEnumerator HealthRegeneration() {

        ReceiveHealth(10);

        yield return new WaitForSeconds(5);
    }

    protected IEnumerator ManaRegeneration() {

        ReceiveMana(10);

        yield return new WaitForSeconds(5);
    }

    protected void GetGameObjectComponents()
    {
        this.Anim = this.CharacterObj.GetComponent<Animator>();
        this.Agent = this.CharacterObj.GetComponent<NavMeshAgent>();
        this.MovementType.GetGameObjectComponents(this.CharacterObj);
    }

    public void SetGameObject(GameObject gameObject)
    {
        this.CharacterObj = gameObject;
        GetGameObjectComponents();
        this.Agent.speed = this.Agent.speed*this.Speed;
    }

    public void SetHealthToMax()
    {
        if (this.Anim)
        {
            Anim.SetBool("isDead", false);
        }
        this.CurrentHealth = this.MaxHealth;
    }
}
