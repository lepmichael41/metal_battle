using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFactory
{
    public CharacterFactory()
    {
    }

    public Character CreateCharacter(int maxHealth, float speed, int maxMana)
    {
        return new Companion(maxHealth, speed, maxMana);
    }

    public Character CreateCharacter(int maxHealth, float speed, int maxMana, int strength, int vitality, int agility, int intelligence)
    {
        return new Hero(maxHealth, speed, maxMana, strength, vitality, agility, intelligence);
    }

    public Character CreateCharacter(int maxHealth, float speed, int maxMana, int experienceReward, int goldReward)
    {
        return new Enemy(maxHealth, speed, maxMana, experienceReward, goldReward);
    }
}
