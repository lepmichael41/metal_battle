using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Hero : Character
{

    public int Gold { get; set; }
    private int Strength { get; set; }
    private int Intelligence { get; set; }
    private int Vitality { get; set; }
    private int Agility { get; set; }

    private int BaseMaxHealth { get; set; }
    private int BaseMaxMana { get; set; }
    private float BaseSpeed { get; set; }
    
    public Enemy target { get; set; }

    private List<Item> listItems = new List<Item>();

    public Hero(int maxHealth, float speed, int maxMana, int strength, int vitality, int agility, int intelligence) {
        this.Strength = strength;
        this.Vitality = vitality;
        this.Agility = agility;
        this.Intelligence = intelligence;

        this.BaseMaxHealth = maxHealth;
        this.MaxHealth = maxHealth;//maxHealth*Mathf.RoundToInt(Mathf.Clamp(this.BaseMaxHealth, this.BaseMaxHealth, this.BaseMaxHealth*(this.Vitality*0.1f)));
        this.CurrentHealth = this.MaxHealth;

        this.BaseSpeed = speed;
        this.Speed = speed;//speed * Mathf.RoundToInt(Mathf.Clamp(this.BaseSpeed, this.BaseSpeed, this.BaseSpeed * (this.Agility * 0.1f)));

        this.BaseMaxMana = maxMana;
        this.MaxMana = maxMana;//maxMana * Mathf.RoundToInt(Mathf.Clamp(this.BaseMaxMana, this.BaseMaxMana, this.BaseMaxMana * (this.Intelligence * 0.1f))); ;
        this.CurrentMana = this.MaxMana;

        this.Level = 1;
        this.Experience = 0;
        this.Gold = 100;

        this.MovementType = new MovementInput();

        UpdateCaller.AddUpdateCallback(Update);

        Ability = new Ability(1, 50, 500, 0, 0);
    }

    protected override void Update()
    {

        GameObject healthBar = GameObject.Find("HealthBar");
        Slider healthBarSlider = healthBar.GetComponentInChildren<Slider>();

        GameObject manaBar = GameObject.Find("ManaBar");
        Slider manaBarSlider = manaBar.GetComponentInChildren<Slider>();

        healthBarSlider.maxValue = this.MaxHealth;
        healthBarSlider.value = this.CurrentHealth;

        manaBarSlider.maxValue = this.MaxMana;
        manaBarSlider.value = this.CurrentMana;

        //CalculateCharacterAttributesItemBonus();

        if (this.IsDead())
        {
            if (this.Anim)
            {

                Anim.SetBool("isMoving", false);
                Anim.SetBool("isAttacking", false);
                Anim.SetBool("isDead", true);
            }
        }
        else
        {
            MovementType.Update();
            GameObject enemyTarget = FindClosestEnemy();
            if (enemyTarget && Anim)
            {
                if ((this.CharacterObj.transform.position - enemyTarget.transform.position).sqrMagnitude <= Ability.range)
                {
                    this.CharacterObj.transform.LookAt(enemyTarget.transform.position);

                    //Enemy target = GameManager.GetInstance().GetHeroTarget(enemyTarget);

                    if (target != null) {

                        this.UseAbility(Ability, target);
                    }
                    Anim.SetBool("isAttacking", true);
                }
                else
                {
                    Anim.SetBool("isAttacking", false);
                }
            }
        }            
    }

    public GameObject FindClosestEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        // If no enemies found at all directly return nothing
        // This happens if there simply is no object tagged "Enemy" in the scene
        if (enemies.Length == 0)
        {
            //Debug.LogWarning("No enemies found!", this.CharacterObj);
            return null;
        }

        GameObject closest;

        // If there is only exactly one anyway skip the rest and return it directly
        if (enemies.Length == 1)
        {
            closest = enemies[0];
            //target.transform.position = closest.transform.position;
            return closest;
        }


        // Otherwise: Take the enemies
        closest = enemies
            // Order them by distance (ascending) => smallest distance is first element
            .OrderBy(go => (this.CharacterObj.transform.position - go.transform.position).sqrMagnitude)
            // Get the first element
            .First();

        //target.transform.position = closest.transform.position;

        return closest;
    }

    public void AddItemToInventory(Item item)
    {
        this.listItems.Add(item);
    }
    public void RemoveItemFromInventory(Item item)
    {
        this.listItems.Remove(item);
    }

    private void CalculateCharacterAttributesItemBonus()
    {
        foreach(Item item in this.listItems)
        {
            if(item is Utility)
            {
                Utility util = (Utility)item;
                this.MaxHealth = this.BaseMaxHealth * Mathf.RoundToInt(
                    Mathf.Clamp(
                        this.BaseMaxHealth, this.BaseMaxHealth, this.BaseMaxHealth * (this.Vitality * 0.1f)*Mathf.Clamp(
                            util.HealthBonus,1,util.HealthBonus)
                        )
                    );
                this.MaxMana = this.BaseMaxMana * Mathf.RoundToInt(
                    Mathf.Clamp(
                        this.BaseMaxMana, this.BaseMaxMana, this.BaseMaxMana * (this.Intelligence * 0.1f)*Mathf.Clamp(
                            util.ManaBonus, 1, util.ManaBonus)
                        )
                    );
                this.Speed = this.BaseSpeed * Mathf.RoundToInt(
                    Mathf.Clamp(
                        this.BaseSpeed, this.BaseSpeed, this.BaseSpeed * (this.Agility * 0.1f) * Mathf.Clamp(
                            util.SpeedBonus, 1, util.SpeedBonus)
                        )
                    );
            }
        }       

    }

    public float CalculateXpMultiplier()
    {
        float multiplier = 1;

        foreach (Item item in this.listItems)
        {
            if (item is Utility)
            {
                Utility util = (Utility)item;
                multiplier += util.XpBonus;

            }
        }

        return multiplier;
    }
    public float CalculateGoldMultiplier()
    {
        float multiplier = 1;

        foreach (Item item in this.listItems)
        {
            if (item is Utility)
            {
                Utility util = (Utility)item;
                multiplier += util.GoldBonus;

            }
        }

        return multiplier;

    }
}