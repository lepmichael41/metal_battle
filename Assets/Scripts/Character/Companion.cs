using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class Companion : Character
{
    public Companion(int maxHealth, float speed, int maxMana)
    {
        this.MaxHealth = maxHealth;
        this.CurrentHealth = this.MaxHealth;

        this.Speed = speed;

        this.MaxMana = maxMana;
        this.CurrentMana = this.MaxMana;

        this.Level = 1;
        this.Experience = 0;

        this.MovementType = new AiMovement();
        UpdateCaller.AddUpdateCallback(Update);
    }

    protected override void Update()
    {
        if (this.IsDead())
        {
            if (this.Anim)
            {
                Anim.SetBool("isDead", true);
            }
        }
        else
        {
            MovementType.Update();
            GameObject enemyTarget = FindClosestEnemy();
            if (enemyTarget && Anim)
            {
                if ((this.CharacterObj.transform.position - enemyTarget.transform.position).sqrMagnitude <= 500f)
                {
                    this.CharacterObj.transform.LookAt(enemyTarget.transform.position);
                    Anim.SetBool("isAttacking", true);
                }
                else
                {
                    Anim.SetBool("isAttacking", false);
                }
            }
        }
    }

    public void SetNavigationTarget(Transform target)
    {
        if (this.Agent)
        {
            this.Agent.enabled = true;
            this.Agent.SetDestination(target.position);
            this.CharacterObj.transform.LookAt(target.position);
        }
    }

    public GameObject FindClosestEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        // If no enemies found at all directly return nothing
        // This happens if there simply is no object tagged "Enemy" in the scene
        if (enemies.Length == 0)
        {
            //Debug.LogWarning("No enemies found!", this.CharacterObj);
            return null;
        }

        GameObject closest;

        // If there is only exactly one anyway skip the rest and return it directly
        if (enemies.Length == 1)
        {
            closest = enemies[0];
            //target.transform.position = closest.transform.position;
            return closest;
        }


        // Otherwise: Take the enemies
        closest = enemies
            // Order them by distance (ascending) => smallest distance is first element
            .OrderBy(go => (this.CharacterObj.transform.position - go.transform.position).sqrMagnitude)
            // Get the first element
            .First();

        //target.transform.position = closest.transform.position;

        return closest;
    }
}