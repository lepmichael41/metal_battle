using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement
{
    public abstract void Update();
    protected abstract void Move();
    public abstract void GetGameObjectComponents(GameObject characterObject);
}
