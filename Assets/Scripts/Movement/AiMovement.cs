using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiMovement : Movement
{
    private NavMeshAgent agent;
    private Transform target;
    private Animator anim;

    // Update is called once per frame
    public override void Update()
    {
        Move();
    }

    protected override void Move()
    {
        if (target)
        {
            agent.SetDestination(target.position);
        }

        if (agent.remainingDistance < 2f)
        {
            agent.isStopped = true;
            anim.SetBool("isMoving", false);
        }
        else
        {
            agent.isStopped = false;
            anim.SetBool("isMoving", true);
        }
    }

    public override void GetGameObjectComponents(GameObject characterObject)
    {
        anim = characterObject.GetComponent<Animator>();
        agent = characterObject.GetComponent<NavMeshAgent>();
    }
}
