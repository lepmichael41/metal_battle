
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class MovementInput : Movement {

	private float zoomSensitivity = 10f;
	private float minCamDistance = 5f;
	private float maxCamDistance = 30f;
	private float cameraDistance = 10f;
	
	private Animator Anim { get; set; }
	private GameObject PlayerObject { get; set; }
	private Camera Cam { get; set; }
	private CharacterController Controller { get; set; }
	private NavMeshAgent Agent { get; set; }

	// Update is called once per frame
	public override void Update () {
		if (Input.GetKey(KeyCode.Mouse1))
        {
			Move();
        }

        if (Agent.remainingDistance < 1f)
        {
			Anim.SetBool("isMoving", false);
		}
        else
        {
			Anim.SetBool("isMoving", true);
		}

		cameraDistance -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
		cameraDistance = Mathf.Clamp(cameraDistance, minCamDistance, maxCamDistance);

		Vector3 targetPostion = new Vector3(PlayerObject.transform.position.x, PlayerObject.transform.position.y + cameraDistance, PlayerObject.transform.position.z + cameraDistance);
		Cam.transform.position = Vector3.Lerp(Cam.transform.position, targetPostion, 0.1f);
	}

	protected override void Move()
    {
		Ray ray = Cam.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit))
		{
			Agent.SetDestination(hit.point);
		}
	}

	public override void GetGameObjectComponents(GameObject characterObject)
	{
		PlayerObject = characterObject;
		Anim = characterObject.GetComponent<Animator>();
		Cam = Camera.main;
		Controller = characterObject.GetComponent<CharacterController>();
		Agent = characterObject.GetComponent<NavMeshAgent>();
	}
}
