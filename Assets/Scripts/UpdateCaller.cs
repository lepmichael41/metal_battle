using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateCaller : MonoBehaviour
{
    private static UpdateCaller instance;

    private Action updateCallback;

    public static void AddUpdateCallback(Action updateMethod)
    {
        if (instance == null)
        {
            instance = new GameObject("[Update Caller]").AddComponent<UpdateCaller>();
        }
        instance.updateCallback += updateMethod;
    }

    public static void RemoveUpdateCallback(Action updateMethod)
    {
        if (instance == null)
        {
            instance = new GameObject("[Update Caller]").AddComponent<UpdateCaller>();
        }
        instance.updateCallback -= updateMethod;
    }

    private void Update()
    {
        updateCallback();
    }
}
