using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base
{
    private int Health {get;set; }

    public int Radius { get; private set; }

    public GameObject BaseObject { get; set; }

    public Base(int health, GameObject baseObject)
    {
        this.Health = health;
        this.Radius = 15;
        this.BaseObject = baseObject;
    }

    public void GetDamage(int amount)
    {
        this.Health -= amount;
    }

    public bool IsDestroyed()
    {
        return this.Health <= 0;
    }
}
