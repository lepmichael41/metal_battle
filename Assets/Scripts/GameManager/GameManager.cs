using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    [SerializeField] private int waves = 0;
    private int heroLives = 5;
    private int heroRespawnTime = 10;

    private List<Character> listCharacter = new List<Character>();
    private Hero hero;
    private Base playerBase;

    private CharacterFactory CharacterFactory { get; set; }
    
    [SerializeField] private Transform playerSpawn;
    [SerializeField] private List<Transform> monsterSpawns = new List<Transform>();

    // sp�cifique unity

    private WaitForSeconds endWaitWaveCountdown = new WaitForSeconds(3);
    private WaitForSeconds endWaitEnemySpawnCountdown = new WaitForSeconds(0.5f);

    [SerializeField] private Animator animator = null;

    [SerializeField] private GameObject BasePrefab;
    [SerializeField] private GameObject HeroPrefab;
    [SerializeField] private GameObject CompanionPrefab;
    [SerializeField] private GameObject SpiderPrefab;
    [SerializeField] private GameObject MeleeFootmanPrefab;
    [SerializeField] private GameObject FlyingBotPrefab;
    [SerializeField] private GameObject BossPrefab;

    [SerializeField] private TMP_Text m_MessageText;

    private bool HeroIsSpawning { get; set; }

    public bool IsAnimating { get; private set; }
    
    public static GameManager GetInstance()
    {
        if (instance == null)
        {
            instance = GameObject.Find("GameManager").GetComponent<GameManager>();
            if (instance == null)
            {
                instance = new GameObject("GameManager").AddComponent<GameManager>();
            }
        }
        return instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameObject canvasObject = GameObject.Find("Canvas_texts");
        m_MessageText = canvasObject.GetComponentInChildren<TMP_Text>();

        this.CharacterFactory = new CharacterFactory();

        HeroIsSpawning = false;
        this.heroLives = 5;
        this.hero = (Hero) this.CharacterFactory.CreateCharacter(500, 1, 1000, 4, 5, 3, 6);
        this.playerBase = new Base(1000, this.BasePrefab);
        SpawnHero();
    }

    // Update is called once per frame
    void Update()
    {
        int companionCount = 0;
        int monsterCount = 0;

        for (int i = this.listCharacter.Count - 1; i >= 0; i--)
        {
            if (listCharacter[i] is Enemy)
            {
                monsterCount++;
                SetEnemyTarget((Enemy)listCharacter[i]);
                if (hero.IsDead())
                {
                    EnemyDestroyBaseIfCloseEnough((Enemy)listCharacter[i]);
                }
            }
            else if(listCharacter[i] is Companion)
            {
                companionCount++;
                SetCompanionMoveTarget((Companion)listCharacter[i]);
            }
        }

        if (this.heroLives < 0)
        {
            this.EndGame();
        }
        if (this.playerBase.IsDestroyed())
        {
            this.EndGame();
        }

        if (hero.IsDead() && !HeroIsSpawning)
        {
            this.HeroIsSpawning = true;
            this.heroLives--;
            StartCoroutine(RespawnHero());
        }

        if (monsterCount == 0 && !IsAnimating)
        {
            this.NextWave();
        }

        if (this.waves % 5 == 0)
        {
            if (companionCount < 1)
            {
                this.SpawnCompanion();
            }
        }
    }

    private void EnemyDestroyBaseIfCloseEnough(Enemy enemy)
    {
        if((this.playerBase.BaseObject.transform.position - enemy.CharacterObj.transform.position).sqrMagnitude <= this.playerBase.Radius)
        {
            this.playerBase.GetDamage(10);
            enemy.RemoveUpdate();
            Destroy(enemy.CharacterObj);
            this.listCharacter.Remove(enemy);
            enemy = null;
        }
    }

    void StartWave()
    {
        //IsAnimating = true;
        //StartCountdown();
        //WaitEndCountdwonWave();

        m_MessageText.enabled = false;
        SpawnMonster();
    }

    void NextWave()
    {
        this.waves++;
        // Get a message based on the scores and whether or not there is a game winner and display it.

        if (m_MessageText)
        {
            m_MessageText.text = "Wave " + this.waves.ToString();
            m_MessageText.enabled = true;
        }

        //StartWave();
        StartCountdown();
    }

    void EndGame()
    {
        if (m_MessageText)
        {
            m_MessageText.text = "You survived " + this.waves.ToString() + " waves";
            m_MessageText.enabled = true;
        }
    }

    void SpawnHero()
    {
        if (this.hero.CharacterObj)
        {
            Destroy(this.hero.CharacterObj);
        }
        SpawnPrefab(this.hero, this.HeroPrefab);
        this.hero.SetHealthToMax();
        this.HeroIsSpawning = false;
    }
    IEnumerator RespawnHero()
    {
        yield return WaitEndCountdownHeroRespawn();
        SpawnHero();
    }

    void SpawnCompanion()
    {
        Character companion = CharacterFactory.CreateCharacter(200, 1, 500);
        listCharacter.Add(companion);
        SpawnPrefab(companion, this.CompanionPrefab);

    }

    void SpawnMonster()
    {
        if (this.waves % 5 == 0)
        {
            if (this.waves % 10 == 0)
            {                                
                StartCoroutine("SpawnBoss");
            }
            else
            {
                StartCoroutine("SpawnSpider");
            }
        }
        else
        {
            //  and waves*1.5 weakling            
            StartCoroutine("SpawnMeleeFootman");
            StartCoroutine("SpawnSpider");
            if(waves > 10)
            {
                StartCoroutine("SpawnFlyingBot");
            }
        }
    }

    private IEnumerator SpawnSpider()
    {        
        Enemy enemy = (Enemy)this.CharacterFactory.CreateCharacter(100, 2, 150, 25, 5);
        this.listCharacter.Add(enemy);
        SpawnPrefab(enemy, this.SpiderPrefab);

        if (waves % 5 == 0 && waves % 10 != 0)
        {
            //spawn  waves*5  weaklings every 5 waves
            for(int i = 1; i<waves*5; i++)
            {
                yield return WaitEndPauseEnnemySpawn();
                enemy = enemy.Clone();
                this.listCharacter.Add(enemy);
                SpawnPrefab(enemy, this.SpiderPrefab);
            }
            
        }
        else
        {
            // else spawn waves*1.5
            for (int i = 1; i < Mathf.CeilToInt(waves * 1.5f); i++)
            {
                yield return WaitEndPauseEnnemySpawn();
                enemy = enemy.Clone();
                this.listCharacter.Add(enemy);
                SpawnPrefab(enemy, this.SpiderPrefab);
            }
        }
    }
    private IEnumerator SpawnMeleeFootman()
    {
        //spawn waves *1.5 footman every waves except 5 and 10
        Enemy enemy = (Enemy)this.CharacterFactory.CreateCharacter(200, 1, 300, 50, 10);
        this.listCharacter.Add(enemy);
        SpawnPrefab(enemy, this.MeleeFootmanPrefab);

        for (int i = 1; i < Mathf.CeilToInt(waves * 1.5f); i++)
        {
            yield return WaitEndPauseEnnemySpawn();
            enemy = enemy.Clone();
            this.listCharacter.Add(enemy);
            SpawnPrefab(enemy, this.MeleeFootmanPrefab);
        }
    }
    private IEnumerator SpawnBoss()
    {
        // spawn waves/10 bosses every 10 waves
        Enemy enemy = (Enemy)this.CharacterFactory.CreateCharacter(1000, 0.5f, 500, 100, 100);
        this.listCharacter.Add(enemy);
        SpawnPrefab(enemy, this.BossPrefab);

        for (int i = 1; i < waves/10 ; i++)
        {
            yield return WaitEndPauseEnnemySpawn();
            enemy = enemy.Clone();
            this.listCharacter.Add(enemy);
            SpawnPrefab(enemy, this.BossPrefab);
        }
    }
    private IEnumerator SpawnFlyingBot()
    {
        // when waves > 10 spawn waves flying
        Enemy enemy = (Enemy)this.CharacterFactory.CreateCharacter(150, 1.5f, 100, 30, 10);
        this.listCharacter.Add(enemy);
        SpawnPrefab(enemy, this.FlyingBotPrefab);

        for (int i = 1; i < waves; i++)
        {
            yield return WaitEndPauseEnnemySpawn();
            enemy = enemy.Clone();
            this.listCharacter.Add(enemy);
            SpawnPrefab(enemy, this.FlyingBotPrefab);
        }
    }

    private void SpawnPrefab(Character character, GameObject prefab)
    {
        GameObject gameObj = null;

        if (character is Hero || character is Companion)
        {
            gameObj = Instantiate(prefab, this.playerSpawn.position, Quaternion.identity);
        }
        else
        {
            int randomint = Random.Range(0, this.monsterSpawns.Count);
            gameObj = Instantiate(prefab, this.monsterSpawns[randomint].position, Quaternion.identity);
        }

        character.SetGameObject(gameObj);
    }

    private void SetEnemyTarget(Enemy enemy)
    {
        if (this.hero.IsDead())
        {
            enemy.SetNavigationTarget(this.playerBase.BaseObject.transform);

            enemy.SetAttackTarget(null);
        }
        else
        {
            enemy.SetNavigationTarget(this.hero.CharacterObj.transform);

            enemy.SetAttackTarget(this.hero);
        }
    }

    private void SetCompanionMoveTarget(Companion companion)
    {
        if (this.hero.IsDead())
        {
            companion.SetNavigationTarget(this.playerSpawn.transform);
        }
        else
        {
            companion.SetNavigationTarget(this.hero.CharacterObj.transform);
        }
    }

    public Enemy GetHeroTarget(GameObject enemyObject) 
    {

        foreach (Character character in this.listCharacter)
        {

            if (character is Enemy) {

                Enemy enemy = (Enemy)character;

                if (enemy.CharacterObj.Equals(enemyObject)) {

                    return enemy;
                }
            }
        }

        return null;
    }

    private void StartCountdown()
    {
        IsAnimating = true;
        animator.Play("Animation_Clip_RoundSystem_Countdown", -1, 0f);
        animator.enabled = true;
    }

    private void CountdownEnded()
    {
        IsAnimating = false;
        animator.enabled = false;
        StartWave();
    }

    private IEnumerator WaitEndCountdwonWave()
    {
        yield return endWaitWaveCountdown;
    }

    private IEnumerator WaitEndCountdownHeroRespawn()
    {
        yield return new WaitForSeconds(this.heroRespawnTime);        
    }

    private IEnumerator WaitEndPauseEnnemySpawn()
    {
        yield return new WaitForSeconds(0.5f);
    }
}
