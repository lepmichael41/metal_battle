using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ability
{
    public int cooldown;
    public int damage;
    public int range;
    public int areaOfEffect;
    public int manaCost;
    public float cooldownCounter;
    public GameObject AudioObj { get; set; }

    public Ability(int cooldown, int damage, int range, int areaOfEffect, int manaCost) {

        this.cooldown = cooldown;
        this.damage = damage;
        this.range = range;
        this.areaOfEffect = areaOfEffect;
        this.manaCost = manaCost;
        this.cooldownCounter = 0;
    }

    public void LaunchAbility() {
        this.cooldownCounter = cooldown;

/*        AudioSource audioClipComponent = AudioObj.GetComponent<AudioSource>();
        audioClipComponent.Play();*/
    }
}
