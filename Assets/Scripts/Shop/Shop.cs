using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop
{
    private ItemFactory ItemFactory { get; set; }

    public Shop()
    {
    }

    
    public void BuyItem(Item item, Hero hero)
    {
        if(IsBuyItemPossible(item, hero.Gold))
        {
            hero.Gold -= item.Price;
            hero.AddItemToInventory(item);
        }
    }
    
    bool IsBuyItemPossible(Item item, int ownedGold)
    {
        if(ownedGold - item.Price < 0)
        {
            return false;
        }
        return true;
    }
}
