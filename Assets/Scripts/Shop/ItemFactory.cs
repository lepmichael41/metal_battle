using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFactory
{
    public ItemFactory()
    {
    }

    Item CreateItem(int price, float healthBonus, float ManaBonus, float attackBonus, float speedBonus, float xpBonus, float goldBonus)
    {
        return new Utility(price, healthBonus, ManaBonus, attackBonus, speedBonus, xpBonus, goldBonus);
    }

    Item CreateItem(int price, int healthAmount, int manaAmount)
    {
        return new Potion(price, healthAmount, manaAmount);
    }

}
