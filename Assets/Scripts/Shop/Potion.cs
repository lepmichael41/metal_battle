using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : Item
{
    public int HealthAmount { get; set; }
    public int ManaAmount { get; set; }

    public Potion(int price, int healthAmount, int manaAmount)
    {
        this.HealthAmount = healthAmount;
        this.ManaAmount = ManaAmount;
        this.Price = price;
    }
}
