using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : Item
{
    public float HealthBonus { get; set; }
    public float ManaBonus { get; set; }
    public float AttackBonus { get; set; }
    public float SpeedBonus { get; set; }
    public float XpBonus { get; set; }
    public float GoldBonus { get; set; }

    public Utility(int price, float healthBonus, float ManaBonus, float attackBonus, float speedBonus, float xpBonus, float goldBonus)
    {
        this.HealthBonus = healthBonus;
        this.ManaBonus = ManaBonus;
        this.AttackBonus = attackBonus;
        this.SpeedBonus = speedBonus;
        this.XpBonus = xpBonus;
        this.GoldBonus = goldBonus;
        this.Price = price;
    }
}
